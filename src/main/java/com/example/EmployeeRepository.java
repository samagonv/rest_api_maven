package com.example;

import org.springframework.data.repository.CrudRepository;

/**
 * Created by Vitali on 03.09.2016.
 */
public interface EmployeeRepository extends CrudRepository<employee, Long> {
}
